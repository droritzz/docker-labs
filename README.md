This project contains different labs to practice Docker.

For most of the exercises run 
- `docker build -t <Container_Name> .`
- `docker container run -d <image ID>`
- `docker container inspect <container ID>`


