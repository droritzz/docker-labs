This directory contains files to run a simple flask app in docker container.

- Dockerfile pulls the image and copies the app
- docker_build.sh creates the image and runs the container

To run the app run `bash -x docker_build.sh`

To view the app in the browser 
- run `docker container inspect <container ID>` to get the IP.
- load in the browser IP:8080