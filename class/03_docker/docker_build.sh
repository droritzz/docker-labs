#!/usr/bin/env bash
##############################################
# created by Silent Mobius for DevOps course #
##############################################

app="docker.flask"

docker build -t ${app} . # DO NOT REMOVE !!! full stop is required

docker run -d -p 8080:80  ${app}
