# Docker Basics

## docker image
- pull container named `nginx`
- inspect the container and write down the hostname given to container
  - check the version of the image and verify that it is latest
  - check what is main command it is running

## docker container
- list all running containers on your system.
- run a container of `busybox` instance
- check if it is running.
  - if not, inspect why it is not running
- create another container based on `nginx`, but run it in daemon mode.
  - inspect the container and write down the ip of it
  - validate the nginx connection with `curl or wget`
  - check logs of container
  - attach your shell to nginx and see if you have access to STDIN/OUT/ERR
    - if nothing is happening, exit container.
  - check that container is still running. no? why?
  - start container with id
  - check `stats` new started container
  - `exec` /bin/bash command on container **interactively**
