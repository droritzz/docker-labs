# Docker Basics

## docker image
- pull container named `nginx`: `docker image pull nginx`
- inspect the container and write down the hostname given to container: `docker container ls | grep 71721e7b42ba` --> vigilant_darwin
- check the version of the image and verify that it is latest: `docker image ls | grep nginx` --> nginx latest
- check what is main command it is running: `docker inspect c3d9582c89f6` --> "Cmd": [
                "nginx",
                "-g",
                "daemon off;"
            ],

## docker container
- list all running containers on your system: `docker container ls`
- run a container of `busybox` instance: `docker container run -d busybox`
- check if it is running: `docker container ls`
  - if not, inspect why it is not running
- create another container based on `nginx`, but run it in daemon mode: `docker run -d nginx`
  - inspect the container and write down the ip of it: `docker container inspect 71721e7b42ba | grep -i IPAddress`
  - validate the nginx connection with `curl or wget`: `curl 172.17.0.3`
  - check logs of container: `docker logs 71721e7b42ba`
  - attach your shell to nginx and see if you have access to STDIN/OUT/ERR: `docker exec -it 71721e7b42ba /bin/bash`
    - if nothing is happening, exit container.
  - check that container is still running. no? why?: `docker container ls | grep 71721e7b42ba`
  - start container with id:`docker run c3d9582c89f6`
  - check `stats` new started container: `docker container stats c3d9582c89f6`
  - `exec` /bin/bash command on container **interactively**: `docker exec -it c3d9582c89f6 /bin/bash`
