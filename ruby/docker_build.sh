#!/usr/bin/env bash
############################################################
# owner: droritzz@gmail.com
# purpose: script that creates docker container to run ROR
# date: 23.6.21
# version: v1.0.0.1
############################################################

#image name
app="docker.ror"

#create the image, mind the DOT - do not delete!!
docker build -t ${app} .

docker run -d -p 8080